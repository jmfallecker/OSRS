﻿using OSRS.Api.Account.Enumerations;
using OSRS.Api.Account.Helpers;
using OSRS.Api.Account.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace OSRS.Api.Account
{
    public class Hiscores : IHiscores
    {
        private ResponseStringToAccountMapper _mapper = new ResponseStringToAccountMapper();
        private Uri EndpointBuilder(string name, AccountType type)
        {
            var accountType = type switch
            {
                AccountType.Regular => "hiscore_oldschool",
                AccountType.Ironman => "hiscore_oldschool_ironman",
                AccountType.Hardcore => "hiscore_oldschool_hardcore_ironman",
                AccountType.Ultimate => "hiscore_oldschool_ultimate",
                AccountType.Deadman => "hiscore_oldschool_deadman",
                AccountType.Seasonal => "hiscore_oldschool_seasonal",
                AccountType.Tournament => "hiscore_oldschool_tournament",
                _ => "hiscore_oldschool",
            };

            var endpoint = $"https://secure.runescape.com/m={accountType}/index_lite.ws?player={name}";

            return new Uri(endpoint);
        }
        /// <summary>
        /// this should be a repository. TODO
        /// </summary>
        private List<IAccount> _cachedAccounts = new List<IAccount>();

        public async Task<IAccount> GetHiscores(string name)
        {
            var whiteHelm = await GetHiscores(name, AccountType.Ultimate);
            bool isWhiteHelm = !Account.IsEmptyAccount(whiteHelm);
            
            var redHelm = await GetHiscores(name, AccountType.Hardcore);
            bool isRedHelm = !Account.IsEmptyAccount(redHelm);            

            var grayHelm = await GetHiscores(name, AccountType.Ironman);
            bool isGrayHelm = !Account.IsEmptyAccount(grayHelm);

            // everyone will show up here
            var regular = await GetHiscores(name, AccountType.Regular);
            bool isRegular = !Account.IsEmptyAccount(regular);

            bool isRetiredIron = isRegular
                && ((isGrayHelm && !grayHelm.Equals(regular))
                    || (isRedHelm && !redHelm.Equals(regular))
                    || (isWhiteHelm && !whiteHelm.Equals(regular)));

            if (isRetiredIron)
                return regular;

            bool isDeadRedHelm = isRedHelm && !redHelm.Equals(grayHelm);
            if (isDeadRedHelm)
                return grayHelm;

            bool isDowngradedWhiteHelm = isWhiteHelm && isGrayHelm;
            if (isDowngradedWhiteHelm)
                return grayHelm;

            if (isWhiteHelm)
                return whiteHelm;

            if (isRedHelm)
                return redHelm;

            if (isGrayHelm)
                return grayHelm;

            return regular;
        }        
        
        public async Task<IAccount> GetHiscores(string name, AccountType accountType)
        {
            var url = EndpointBuilder(name, accountType);

            using var client = new HttpClient();           
            HttpResponseMessage response = await client.GetAsync(url);

            if (!response.IsSuccessStatusCode) return Account.EmptyAccount;

            return _mapper.Map(await response.Content.ReadAsStringAsync());
        }

        public async Task<IAccount> GetHiscores(string name, AccountType accountType, bool forceRefresh)
        {
            IAccount account;

            var isCached = DoesCacheContainAccount(name);

            account = isCached ? GetCachedAccount(name) : await GetHiscores(name, accountType);

            if (forceRefresh)
            {
                account = await GetHiscores(name, accountType);
                UpdateCache(name, account);
            }            

            return account;
        }

        private void UpdateCache(string displayName, IAccount account)
        {
            if (!_cachedAccounts.Any(acc => acc.DisplayName.Equals(displayName)))
                _cachedAccounts.Add(account);

            var cachedAccount = _cachedAccounts.Single(acc => acc.DisplayName.Equals(displayName));

            _cachedAccounts.Remove(cachedAccount);
            _cachedAccounts.Add(account);
        }
        private bool DoesCacheContainAccount(string displayName) => _cachedAccounts.Any(acc => acc.DisplayName.Equals(displayName));
        private IAccount GetCachedAccount(string displayName) => _cachedAccounts.FirstOrDefault(acc => acc.DisplayName.Equals(displayName));
    }
}
