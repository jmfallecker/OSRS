﻿using System;

namespace OSRS.Api.Account.Interfaces
{
    public interface ISkill : IEquatable<ISkill>
    {
        string Name { get; set; }
        int Rank { get; set; }
        int Level { get; set; }
        int Experience { get; set; }
    }
}
