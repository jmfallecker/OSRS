﻿using OSRS.Api.Account.Enumerations;
using OSRS.Api.Account.Interfaces;

namespace OSRS.Api.Account
{
    public class Account : IAccount
    {
        public static IAccount EmptyAccount = new Account();
        public static bool IsEmptyAccount(IAccount account) => account == EmptyAccount;

        public AccountType AccountType { get; set; }
        public string DisplayName { get; set; }
        public ISkill Total { get; set; }
        public ISkill Attack { get; set; }
        public ISkill Defense { get; set; }
        public ISkill Strength { get; set; }
        public ISkill Hitpoints { get; set; }
        public ISkill Ranged { get; set; }
        public ISkill Prayer { get; set; }
        public ISkill Magic { get; set; }
        public ISkill Cooking { get; set; }
        public ISkill Woodcutting { get; set; }
        public ISkill Fletching { get; set; }
        public ISkill Fishing { get; set; }
        public ISkill Firemaking { get; set; }
        public ISkill Crafting { get; set; }
        public ISkill Smithing { get; set; }
        public ISkill Mining { get; set; }
        public ISkill Herblore { get; set; }
        public ISkill Agility { get; set; }
        public ISkill Thieving { get; set; }
        public ISkill Slayer { get; set; }
        public ISkill Farming { get; set; }
        public ISkill Runecraft { get; set; }
        public ISkill Hunter { get; set; }
        public ISkill Construction { get; set; }

        public bool Equals(IAccount other)
        {
            return DisplayName.Equals(other.DisplayName)
                && Total.Equals(other.Total)
                && Attack.Equals(other.Attack)
                && Defense.Equals(other.Defense)
                && Strength.Equals(other.Strength)
                && Hitpoints.Equals(other.Hitpoints)
                && Ranged.Equals(other.Ranged)
                && Prayer.Equals(other.Prayer)
                && Magic.Equals(other.Magic)
                && Cooking.Equals(other.Cooking)
                && Woodcutting.Equals(other.Woodcutting)
                && Fletching.Equals(other.Fletching)
                && Fishing.Equals(other.Fishing)
                && Firemaking.Equals(other.Firemaking)
                && Crafting.Equals(other.Crafting)
                && Smithing.Equals(other.Smithing)
                && Mining.Equals(other.Mining)
                && Herblore.Equals(other.Herblore)
                && Agility.Equals(other.Agility)
                && Thieving.Equals(other.Thieving)
                && Slayer.Equals(other.Slayer)
                && Farming.Equals(other.Farming)
                && Runecraft.Equals(other.Runecraft)
                && Hunter.Equals(other.Hunter)
                && Construction.Equals(other.Construction);
        }
    }
}
