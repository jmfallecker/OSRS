﻿namespace OSRS.Domain

/// Tools for manipulating skills
module Skill =

    type Experience = Experience of uint
    type Level = Level of uint
    let maxExperience = Experience 200000000u

    type Skills =
        | Attack
        | Agility
        | Construction
        | Cooking
        | Crafting
        | Defence
        | Farming
        | Firemaking
        | Fishing
        | Fletching
        | Herblore
        | Hitpoints
        | Hunter
        | Magic
        | Mining
        | Prayer
        | Ranged
        | Runecrafting
        | Slayer
        | Smithing
        | Strength
        | Thieving
        | Woodcutting

    type Player = Player
    type Account = Account
    type NPC = NPC

    let levels = seq { for i in 1u .. 99u -> Level i }
    let experienceBetweenLevels = 
        [
            Experience 83u
            Experience 91u
            Experience 102u
            Experience 112u
            Experience 124u
            Experience 138u
            Experience 151u
            Experience 168u
            Experience 185u
            Experience 204u
            Experience 226u
            Experience 249u
            Experience 274u
            Experience 304u
            Experience 335u
            Experience 369u
            Experience 408u
            Experience 450u
            Experience 497u
            Experience 548u
            Experience 606u
            Experience 667u
            Experience 737u
            Experience 814u
            Experience 898u
            Experience 990u
            Experience 1094u
            Experience 1207u
            Experience 1332u
            Experience 1470u
            Experience 1623u
            Experience 1791u
            Experience 1977u
            Experience 2182u
            Experience 2409u
            Experience 2658u
            Experience 2935u
            Experience 3240u
            Experience 3576u
            Experience 3947u
            Experience 4358u
            Experience 4810u
            Experience 5310u
            Experience 5863u
            Experience 6471u
            Experience 7144u
            Experience 7887u
            Experience 8707u
            Experience 9612u
            Experience 10612u
            Experience 11715u
            Experience 12934u
            Experience 14278u
            Experience 15764u
            Experience 17404u
            Experience 19214u
            Experience 21212u
            Experience 23420u
            Experience 25856u
            Experience 28546u
            Experience 31516u
            Experience 34795u
            Experience 38416u
            Experience 42413u
            Experience 46826u
            Experience 51699u
            Experience 57079u
            Experience 63019u
            Experience 69576u
            Experience 76818u
            Experience 84812u
            Experience 93638u
            Experience 103383u
            Experience 114143u
            Experience 126022u
            Experience 139138u
            Experience 153619u
            Experience 169608u
            Experience 187260u
            Experience 206750u
            Experience 228269u
            Experience 252027u
            Experience 278259u
            Experience 307221u
            Experience 339198u
            Experience 374502u
            Experience 413482u
            Experience 456519u
            Experience 504037u
            Experience 556499u
            Experience 614422u
            Experience 678376u
            Experience 748985u
            Experience 826944u
            Experience 913019u
            Experience 1008052u
            Experience 1112977u
            Experience 1228825u
            Experience 0u // this could be added later if we want to include 200M
        ]

    let experienceUntilNextLevel = Map.ofSeq (Seq.zip levels experienceBetweenLevels)
        
    type SkillType = All | Combat | NonCombat

    let private Combat = [| 
        Attack
        Strength 
        Defence 
        Ranged 
        Magic  
        Prayer  
        Hitpoints 
    |]    
    let private NonCombat = [|
        Runecrafting
        Agility 
        Herblore 
        Thieving 
        Crafting 
        Fletching 
        Slayer 
        Hunter 
        Mining 
        Smithing 
        Fishing 
        Cooking 
        Firemaking 
        Woodcutting 
        Farming 
        Construction
    |]

    let randomSkill (excludedSkills: Skills list) =
        let random = new System.Random()
        fun skillType ->
            let skills = List.ofArray (
                            match skillType with    
                            | All -> (Array.append Combat NonCombat)
                            | Combat -> Combat
                            | NonCombat -> NonCombat)

            let skillsWithoutExcludes = List.except excludedSkills skills
            skillsWithoutExcludes.[random.Next(skills.Length - excludedSkills.Length)]

/// This is a module specific to the Wilderness Podcast discord, etc.
module Wild =
    open Skill    

    let skillsAlreadyUsed = [
        Skills.Farming // week 1 - 6/14 - 6/20
        Skills.Fishing // week 2 - 6/21 - 6/27
        Skills.Hunter // week 3 - 6/28 - 7/4
        Skills.Woodcutting // week 4 
        Skills.Slayer // week 5
        Skills.Prayer // week 6
        Skills.Herblore // week 7
        ]

    let sotw = fun () -> randomSkill skillsAlreadyUsed All
        
        
        
/// Dedicated to understanding the Organized Crime books usage
module OrganizedCrime =
    open Skill

    let ExpFromTrainingManual currentLevel = 
        let (Level levelValue) = currentLevel
        25u * levelValue

    let TM = ExpFromTrainingManual

    let FullExperienceUntilNextLevel startingLevel =     
        let (Experience exp) = experienceUntilNextLevel.[startingLevel]
        exp
    
    let ExactAmountTrainingManualsNeededToCompleteLevel level = ((FullExperienceUntilNextLevel level) |> double) / ((TM level) |> double)    

    let RoundedAmountOfTrainingManuals level = ceil (ExactAmountTrainingManualsNeededToCompleteLevel level) |> uint

    let ExperienceFromLastTrainingManualUsed level = RoundedAmountOfTrainingManuals level * TM level - FullExperienceUntilNextLevel level